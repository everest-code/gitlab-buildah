#!/bin/bash
# author: SGT911

if [ ! -d "./.git" ]; then
    echo 'ERROR: Not in a git repository'
    exit 1
fi

LIB=$(dirname $(which $0))/lib

source $LIB/taging.sh

push() {
    IMG_DIR=$1
    RELEASE_OLD=$($LIB/get_release.sh $IMG_DIR $DEFAULT_TAGVER)
    RELEASE_NEW=$($LIB/get_release.sh $IMG_DIR latest)

    buildah tag $RELEASE_OLD $RELEASE_NEW
}

for ctx in $($LIB/list_contexts.sh $PWD); do
    echo "Retaging context \"${ctx}\""
    push ${ctx}
done