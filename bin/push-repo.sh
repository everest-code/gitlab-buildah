#!/bin/bash
# author: SGT911

if [ ! -d "./.git" ]; then
    echo 'ERROR: Not in a git repository'
    exit 1
fi

LIB=$(dirname $(which $0))/lib

source $LIB/taging.sh

push() {
    IMG_DIR=$1
    RELEASE=$($LIB/get_release.sh $IMG_DIR $DEFAULT_TAGVER)

    buildah push $RELEASE
}

for ctx in $($LIB/list_contexts.sh $PWD); do
    echo "Pushing context \"${ctx}\""
    push ${ctx}
done