#!/bin/bash
# author: SGT911

if [ ! -d "./.git" ]; then
    echo 'ERROR: Not in a git repository'
    exit 1
fi

LIB=$(dirname $(which $0))/lib

source $LIB/taging.sh

build() {
    IMG_DIR=$1

    RELEASE=$($LIB/get_release.sh $IMG_DIR $DEFAULT_TAGVER)
    CONTAINERFILE=${IMG_DIR}/Containerfile
    CONTAINERIGNOREFILE=${IMG_DIR}/.containerignore

    flags="--label commit=$COMMIT --label branch=$BRANCH"
    if [ "$TAG" != "$BRANCH" ] && [ "$TAG" != "" ]; then
        flags="$flags --label tag=$TAG"
    fi

    if [ -f "$CONTAINERIGNOREFILE" ]; then
        flags="--ignorefile $CONTAINERIGNOREFILE"
    fi

    buildah bud -t $RELEASE -f $CONTAINERFILE $flags .

    if [ "$?" -ne "0" ]; then
        exit 1
    fi
    echo "Image \"${RELEASE}\" created"
}

for ctx in $($LIB/list_contexts.sh $PWD); do
    echo "Building context \"${ctx}\""
    build ${ctx}
done