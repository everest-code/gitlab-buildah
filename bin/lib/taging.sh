#!/sin/bash

if [ "$CI" = "true" ]; then
    TAG=$CI_COMMIT_TAG
    COMMIT=$CI_COMMIT_SHORT_SHA
    BRANCH=$CI_COMMIT_BRANCH
else
    TAG=$(git describe --tags --abbrev=0 2> /dev/null || git branch --show-current)
    COMMIT=$(git rev-parse --short HEAD)
    BRANCH=$(git branch --show-current)
fi

DEFAULT_TAGVER=ci-${COMMIT}

if [ "$1" = "latest" ]; then
    DEFAULT_TAGVER=latest
else
    if [ "$1" = "tag" ]; then
        DEFAULT_TAGVER=${TAG}
    else
        if [ "$1" = "branch" ]; then
            DEFAULT_TAGVER=b-${BRANCH}
        fi
    fi
fi