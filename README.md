# Gitlab CI-CD buildah

Gitlab CI/CD Image with Buildah


Simple implementation in **Gitlab CI/CD**
```yaml
stages:
  - build

build:
  stage: build
  only:
    - main
  image: registry.gitlab.com/everest-code/gitlab-buildah:latest
  script:
    - login.sh -u $CI_REGISTRY_USER -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - build-repo.sh commit
    - retag-latest.sh commit
    - push-repo.sh latest
```